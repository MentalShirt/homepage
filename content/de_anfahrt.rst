menu: Anfahrt
group: index
order: 3
template: base.html


Anfahrt
=======

.. raw:: html

   <p>
   <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=9.580078125%2C52.07022125944487%2C11.633148193359377%2C52.8525471567007&amp;layer=mapnik&amp;marker=52.46312198241156%2C10.606613159179688" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=52.4631&amp;mlon=10.6066#map=10/52.4631/10.6066">Größere Karte anzeigen</a></small><br>
   Das Hacken Open Air findet am <a href="https://osm.org/go/0G7B2YRAL?m=" >
   Pfadfinderheim Welfenhof</a> in Gifhorn
   - ca. 25km nördlich von Braunschweig - statt.
   </p>
   <p>
   Die Adresse für Navigationsgeräte ist:<br>
   <address>
   III. Koppelweg 6<br>
   38518 Gifhorn<br>
   </address>
   </p>
