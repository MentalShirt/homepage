import os

class Singlepage:

    def contents_parsed(self, context):
    
        for content in context.contents:
        
            if content["group"]:
                if content["group"] in os.path.basename(content["path"]):
                    print("Found group-master page: {}".format(content["path"]))
                    members = context.contents.filter(group=content["group"]).exclude(path=content["path"]).order_by("order")
                    menu = []
                    content["content_body"] = "<div id='pagemenu_top'>" + content["content_body"] + "</div>"
                    for member in members:
                        content["content_body"] += "<div id='pagemenu_{}'>\n{}\n</div>".format(member["menu"], member["content_body"])
                        menu.append(("pagemenu_{}".format(member["menu"]), member["menu"]))

                    content["pagemenu"] = menu
