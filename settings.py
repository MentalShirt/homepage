import os
import flamingo

PLUGINS = [
    'flamingo.plugins.Redirects',
    'flamingo.plugins.rstBootstrap3',
    'flamingo.plugins.rstFile',
    'flamingo.plugins.rstPygments',
    'plugins/title.py::Title',
]

@flamingo.hook("contents_parsed")
def fix_headings(context):
    for content in context.contents:
        cb = content["content_body"]
        cb = cb.replace("h3>", "h4>")
        cb = cb.replace("h2>", "h3>")
        content["content_body"] = cb

POST_BUILD_LAYERS = [
        'static',
]
